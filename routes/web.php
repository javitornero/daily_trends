<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/api')->group(function () {
    Route::get('/getFeedsOfToday', 'FeedController@getFeedsOfToday');
});

Auth::routes();

/**
 * Requires authentication.
 */
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/feeds', 'FeedController@index')->name('feeds.index');
    Route::get('/feeds/datatable', 'FeedController@datatable')->name('datatable');
    Route::post('/feeds/show', 'FeedController@show')->name('feeds.show');
    Route::post('/feeds/store', 'FeedController@store')->name('feeds.store');
    Route::post('/feeds/destroy', 'FeedController@destroy')->name('feeds.destroy');
    Route::get('/feeds/scrap', 'FeedController@scrap')->name('feeds.scrap');
});
