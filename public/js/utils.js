
/////////////////////////////////////////////////////////////////////////////
// Tooltip inicialization
$("body").tooltip({
    selector: '[data-toggle="tooltip"]'
});


/////////////////////////////////////////////////////////////////////////////
// Get uri prefix
function uriPrefix(){
    var uri_splited = location.pathname.substr(1).split('/');
    var n_pos = parseInt(uri_splited.length)-1;
    var uriPrefix = uri_splited[n_pos];

    return uriPrefix;
}

var tree_view = $('#a_'+uriPrefix()).parent().parent().parent().attr('id');
var drop_menu = $('#'+tree_view).children().attr('id');
$('#a_'+uriPrefix()).removeClass('bg-gradient-info text-white').addClass('active');
$('.main_link > #a_'+uriPrefix()).addClass('bg-gradient-info text-white');
$('#'+tree_view).addClass('menu-open');
$('#'+drop_menu).addClass('bg-gradient-info text-white');


/////////////////////////////////////////////////////////////////////////////
//
$("#app").on('click','button.btn_reset_fm', function() {
    $(".div_buttons_reset").hide();
    $(".div_buttons").hide();
    $(".ul_error").html('').hide();
    $(".div_buttons_store").fadeIn(2000);
});

$('.modal').on('show.bs.modal', function () {
    $('.lb_name').html('');
    $(".div_buttons_reset").hide();
    $(".div_buttons").hide();
    $(".div_buttons_ok").hide();
    $(".ul_error").html('').hide();
    $(".div_buttons_store").hide();
    $('#fmControlFile1').hide();
    $('#show_feed_modal .form-control').prop("disabled", true);
    $( ".invalid-feedback" ).remove();
    $(".form-control").removeClass('is-invalid');
    $('body').addClass('sidebar-collapse');
    $(".reset_select option:selected").prop("selected", false);
    $('.reset_select option[value=0]').prop('selected', true);
    $('.reset_number').val('0');
    $('.reset_txt').val('');
    $('#published_date').html('<input class="form-control col-6 reset_dts" type="date" id="fmControlDate1" disabled><input class="form-control offset-1 col-5 reset_dts" type="time" id="fmControlTime1" disabled>');
    $('#img_feed_detail').attr('src', 'images/default_feed.jpg');
    $(".div_buttons").hide();
    $(".div_buttons").fadeIn( 2000 );
});

$('.modal').on('shown.bs.modal', function () {
    $( this ).find('.nav-link').removeClass("active");
    $( this ).find('.nav-link').first().addClass("active");
    $( this ).find('.tab-pane').removeClass("active");
    $( this ).find('.tab-pane').first().addClass("active");
    $( this ).find('button').first().focus();
});
