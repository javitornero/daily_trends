<!-- ////////////////// MODAL ///////////////////////// -->
<div class="container">
    <div id="show_feed_modal" class="modal fade">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <section class="content no_padding_under_sm">
                        <div class="container-fluid no_padding margin_top">
                            <div class="row no-gutters">
                                <div class="col-12 col-sm-6">
                                    <div class="d-flex justify-content-center">
                                        <img id="img_feed_detail" class="img_feed_detail elevation-2">
                                    </div>
                                    <div class="mt-2 form-group">
                                        <form action="{{ url('/feeds/store') }}" enctype="multipart/form-data" method="POST" id="fm_image_store">
                                            <input type="file" class="form-control-file display_none reset_txt" id="fmControlFile1" name="image" accept="image/*">
                                        </form>
                                    </div>
                                </div>
                                <input class="form-control reset_txt" type="hidden" id="fmControlText0" disabled>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="fmControlText2">Publicador</label>
                                        <input class="form-control reset_txt" type="text" id="fmControlText1" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="fmControlText3">Fuente</label>
                                        <input class="form-control reset_txt" type="text" id="fmControlText2" disabled>
                                    </div>
                                    <div>
                                        <div class="form-group">
                                            <label for="fmControlDate1">Actualizado en</label>
                                            <div class="row no-gutters" id="published_date">
                                                <input class="form-control col-6 reset_dts" type="date" id="fmControlDate1" disabled>
                                                <input class="form-control offset-1 col-5 reset_dts" type="time" id="fmControlTime1" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fmControlTextarea1">Título</label>
                                <textarea class="form-control reset_txt" id="fmControlTextarea1" rows="1" disabled></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fmControlTextarea2">Subtítulo</label>
                                <textarea class="form-control reset_txt" id="fmControlTextarea2" rows="2" disabled></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fmControlTextarea3">Cuerpo</label>
                                <textarea class="form-control reset_txt" id="fmControlTextarea3" rows="4" disabled></textarea>
                            </div>
                        </div>
                    </section>

                </div>

                <div class="modal-footer">
                    <div class="div_buttons">
                        <button id="btn_feed_edit" class="btn btn-info btn_feed_edit">Editar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                    <div class="div_buttons_store display_none">
                        <button id="btn_store_feed" class="btn btn-info">Guardar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                    <div class="div_buttons_ok display_none w_100percent text-right">
                        <div class="alert alert-success text-center w_100percent" role="alert"></div>
                        <button id="btn_feed_edit" class="btn btn-info btn_feed_edit">Volver a editar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                    <div class="div_buttons_reset display_none w_100percent text-right">
                        <div><ul class="display_none ul_error list-unstyled w_100percent text-center" role="alert"></ul></div>
                        <button class="btn btn-info btn_reset_fm">Volver a editar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ////////////////// END MODAL
