<!-- ////////////////// MODAL ///////////////////////// -->
<div class="container">
    <div id="destroy_feed_modal" class="modal fade">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <section class="content no_padding_under_sm">
                        <div class="container-fluid no_padding margin_top">

                            <input type="hidden" id="id_user" class="id_to_action">
                            <label><small class='text-danger'>Atención</small>: Si continua, eliminará este feed.</label><br>
                            <p>Si realmente desea continuar, haga click en Eliminar.</p>

                            <div><ul class="display_none text_center ul_error list-unstyled" role="alert"></ul></div>

                        </div>
                    </section>

                </div>

                <div class="modal-footer">
                    <div class="div_buttons">
                        <button id="btn_destroy_feed" class="btn btn-danger">Eliminar</button>
                        <button class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                    <div class="div_buttons_ok display_none w_100percent text-right">
                        <div class="alert alert-success text-center w_100percent" role="alert"></div>
                        <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ////////////////// END MODAL
