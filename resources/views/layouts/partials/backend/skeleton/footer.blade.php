<footer class="main-footer">
  <div class="float-left d-none d-sm-inline-block p-0" style="line-height: 0.5rem">
   <small>Bajo licencia
    <a href="https://creativecommons.org/licenses/by-nc-nd/3.0/es/" target="_blank">
      Creative Commons: [CC BY-NC-ND 3.0 ES]
    </a></small>
  </div>
  <div class="float-right d-none d-sm-inline-block" style="line-height: 0.5rem">
    {{--<b>Version</b> 1.0.0-beta--}}
      <small>Javi Tornero Montellano</small>
  </div>
</footer>
