<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
  <div class="p-3">
    <p>Festivos:</p>
    <div id="my_dates">
        @foreach ($data['event_dates'] as $event_date)
          <p class="text-center">{{ $event_date }}</p>
        @endforeach
    </div>
  </div>
</aside>
<!-- /.control-sidebar -->