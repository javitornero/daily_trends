<!-- /.breadcrumbs -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark capitalize page_title"></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <nav aria-label="breadcrumb">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item capitalize"><a href="{{ route('home') }}">{{ trans('contents.home') }}</a></li>
      </ol>
    </nav>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->