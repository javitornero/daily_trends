<!-- Sidebar Menu -->
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
        <li class="nav-item main_link">
            <a href="{{ route('home') }}" class="nav-link" id="a_home">
                <i class="nav-icon fa fa-home"></i>
                <p class="capitalize">
                    Inicio
                </p>
            </a>
        </li>
        @if(Auth::check())
        <li class="nav-item has-treeview main_link" id="master_tree_view">
            <a href="#" class="nav-link" id="a_masters_drop_menu">
                <i class="nav-icon fas fa-database"></i>
                <p class="capitalize">
                    Gestión
                    <i class="right fa fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('feeds.index') }}" class="nav-link" id="a_feeds">
                        {{--<i class="left fas fa-circle" style="font-size: 0.4rem;"></i>--}}
                        <i class="fas fa-long-arrow-alt-right"></i>
                        <i class="nav-icon fas fa-boxes"></i>
                        <p class="capitalize">
                            Feeds
                        </p>
                    </a>
                </li>
            </ul>
        </li>
        @endif
    </ul>
</nav>
<!-- /.sidebar-menu -->
