<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 no-mobile" style="background-color: #334e70;z-index: 9001;">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link d-flex justify-content-around">
        <img src="/images/logo_trans.png" alt="Daily Trends" style="border: solid 1px white; border-radius: 5px">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        @include('layouts.partials.backend.skeleton.sidebar-menu')
    </div>
    <!-- /.sidebar -->
</aside>
