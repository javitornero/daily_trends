<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav no-mobile">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('home') }}" class="nav-link capitalize">Inicio</a>
        </li>
    </ul>
    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{--<span id="span_avatar_img">
                     @if (Auth::user()->avatar)
                      <img src="{{ Auth::user()->avatar }}" class="md-avatar img-circle elevation-2" alt="" id="avatar_img_header">
                     @else
                       <img src="images/user_default.png" class="md-avatar img-circle elevation-2" alt="">
                     @endif
                    </span>--}}
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    {{--<a class="dropdown-item text_center" href="{{ route('profile') }}">
                      <i class="nav-icon far fa-user" style="position: absolute; left: 0; margin: 0.4rem; margin-left: 0.6rem"></i>
                      Perfil
                    </a>--}}
                    <a class="dropdown-item text_center" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-power-off" style="position: absolute; left: 0; margin: 0.3rem; margin-left: 0.6rem"></i>
                        {{--{{ __('Logout') }}--}}
                        Cerrar sesión
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
</nav>
<!-- /.navbar -->
