<!DOCTYPE html>
<html lang="{{ App::getLocale() }}" style="height:100%;">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }} | @yield('title-sufix')</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('header-extras')
    </head>

    <body class="hold-transition sidebar-collapse sidebar-mini" id="myApp">
        <div class="wrapper" id="app">
            @include('layouts.partials.backend.skeleton.header')
            @include('layouts.partials.backend.skeleton.sidebar')
            <div class="content-wrapper pt-4 full">
                {{--@include('layouts.partials.backend.skeleton.breadcrumbs')--}}
                {{--@include('flash::message')--}}
                @yield('content')
            </div>
            @include('layouts.partials.backend.modals.show_feed_modal')
            @include('layouts.partials.backend.modals.destroy_feed_modal')
            <!-- /.content-wrapper -->
            @include('layouts.partials.backend.skeleton.footer')
        </div> <!-- ./wrapper -->

        @yield('footer-prev-extras')

        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/utils.js') }}"></script>

        @yield('footer-extras')
    </body>

</html>
