@extends('layouts.backend')

@section('title-sufix')
    Inicio
@endsection

@section('header-extras')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>¡Bienvenido!, estás dentro :)</p>
                    <p>Puedes gestionar los feeds desde <a href="{{ route('feeds.index') }}">aquí</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    <script type="text/javascript">
        (function ($) {

        })(jQuery);
    </script>
@endsection
