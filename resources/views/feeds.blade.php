@extends('layouts.backend')

@section('title-sufix')
    Inicio
@endsection

@section('header-extras')
@endsection

@section('content')
    <section class="content no_padding_under_sm">
        <div class="container-fluid no_padding margin_top">
            <div class="table-responsive pt-1">
                <table id="feeds_table" class="my_table table table-bordered">
                    <thead>
                    <tr>
                        <th data-priority="1" scope="col">imagen</th>
                        <th data-priority="1" scope="col">fecha</th>
                        <th data-priority="-1" scope="col">publicador</th>
                        <th scope="col">fuente</th>
                        <th scope="col">titulo</th>
                        <th scope="col">cuerpo</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">fecha</th>
                        <th scope="col">publicador</th>
                        <th scope="col">fuente</th>
                        <th scope="col">título</th>
                        <th scope="col">cuerpo</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    <script type="text/javascript">
        $("#app").on('click','button#btn_destroy_feed', function() {
            var myJsonData = '{"id":"'+id+'"}';
            myJsonData = JSON.parse(myJsonData);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/feeds/destroy',
                data:myJsonData,
                dataType: 'json',
            }).done(function(result) {
                $(".div_buttons").hide();
                $(".div_buttons_ok").fadeIn(2000);
                $(".alert-success").html(result.success);
                feeds_table.ajax.reload();
            }).fail(function(err) {
                console.log(err);
            });
        });

        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = (function(theFile) {
                    var image = new Image();
                    image.src = theFile.target.result;

                    image.onload = function() {
                        // access image size here
                        console.log(this.width);
                        console.log(this.height);
                        console.log(input.files[0].size);
                        if (input.files[0].size <= 2097152){
                            $('#img_feed_detail').attr('src', this.src);
                        } else {
                            alert('El tamaño de la imagen excede el límite de 2Mb');
                        }
                    };
                });

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fmControlFile1").on('change', function(){
            readURL(this);
        });

        $("#fm_image_store").submit(function(e){
            e.preventDefault();
        });

        $("#app").on('click','button#btn_store_feed', function() {
            /*.replace(/"/g, '\\"')*/
            published_date = $('#fmControlDate1').val();
            published_time = $('#fmControlTime1').val();

            var formData = new FormData($('#fm_image_store')[0]);
            feed_id = $('#fmControlText0').val();
            if(feed_id.length > 0){
                formData.append('id', feed_id);
            }
            formData.append('img_src', $('#img_feed_detail').attr('src'));
            formData.append('publisher', $('#fmControlText1').val());
            formData.append('source', $('#fmControlText2').val());
            formData.append('published_date', moment(published_date+' '+published_time).format('YYYY-MM-DD HH:MM:SS'));
            formData.append('title', $('#fmControlTextarea1').val());
            formData.append('subtitle', $('#fmControlTextarea2').val());
            formData.append('body', $('#fmControlTextarea3').val());

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: "/feeds/store",
                data:formData,
                dataType: 'json',
                processData: false,
                contentType: false,
            }).done(function(result) {
                $('#fmControlFile1').hide();
                $('#feed_show_modal .form-control').prop("disabled", true);
                $(".div_buttons_store").hide();
                $(".alert-success").html(result.success);
                $(".div_buttons_ok").fadeIn(200);
                setFeedModalWithResult(result.feed);
                feeds_table.ajax.reload();
            }).fail(function(err) {
                console.log(err);
                var errorsObj = err.responseJSON.errors;
                $.each( errorsObj, function( key, value ) {
                    $(".ul_error").append('<li class="text-danger">'+key + ": "+value+'</li>');
                });
                $(".ul_error").fadeIn('slow');
                $(".div_buttons_store").hide();
                $(".div_buttons_reset").show();
            });
        });

        $("#app").on('click','button.btn_feed_edit', function() {
            $(".div_buttons").hide();
            $(".div_buttons_ok").hide();
            $(".alert-success").html('');
            $(".div_buttons_store").fadeIn( 'slow');
            $('#show_feed_modal .form-control').prop("disabled", false);
            $('#fmControlText1').focus();
            $('#fmControlFile1').fadeIn('slow');
        });

        function setFeedModalWithResult(result) {
            $('#fmControlText0').val(result.id);
            $('#img_feed_detail').attr('src', result.image);
            $('#fmControlText1').val(result.publisher);
            $('#fmControlText2').val(result.source);
            $('#fmControlDate1').val(moment(result.published_date).format("YYYY-MM-DD"));
            $('#fmControlTime1').val(moment(result.published_date).format("hh:mm:ss"));
            $('#fmControlTextarea1').val(result.title);
            $('#fmControlTextarea2').val(result.subtitle);
            $('#fmControlTextarea3').val(result.body);
        }

        function getFeedById(id) {
            var myJsonData = '{"id":"'+id+'"}';
            myJsonData = JSON.parse(myJsonData);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                url: '/feeds/show',
                data:myJsonData,
                dataType: 'json',
            }).done(function(result) {
                setFeedModalWithResult(result.feed);
            }).fail(function(err) {
                console.log(err);
            });
        }

        function scrap() {
            $.ajax({
                url: "feeds/scrap",
                method: 'GET',
            }).done(function(result) {
                console.log(result.success);
            }).fail(function(err) {
                console.log(err);
            });
        }

        (function ($) {
            // Setup - add a text input to each footer cell
            $('#feeds_table tfoot th').each( function () {
                var title = $(this).text();
                if(title != ''){
                    $(this).html('<input type="text" class="mySearch" placeholder="' + title + '" />');
                }
            } );
            feeds_table = $('#feeds_table').DataTable({
                /*"fnRowCallback": function(nRow) {
                    console.log("fnRowCallback");
                    $(nRow).on('mousedown', function (e) {
                        if (e.button == 2) {
                            console.log('Right mouse button!');
                            return false;
                        }
                        return true;
                    });
                },*/
                "order": [[ 1, "desc" ]],
                /*select: {
                    style: 'multi'
                },*/
                rowId: 'id',
                "ajax": {
                    "type": "GET",
                    "url": "/feeds/datatable",
                    // error callback to handle error
                    "error": function (reason) {
                        console.log("Error occurred !");
                        console.log(reason);
                        // parse "reason" here and take appropriate action
                    }
                },
                "columns": [
                    {data: 'image', name: 'image'},
                    {data: 'published_date', name: 'published_date'},
                    {data: 'publisher', name: 'publisher'},
                    {data: 'source', name: 'source'},
                    {data: 'title', name: 'title'},
                    {data: 'body', name: 'body'},
                ],
                "columnDefs": [
                    { "sClass": "colHiddenAtXS", "aTargets": [ 1,2,5 ] },
                    {
                        "render": function (data, type, row) {
                            return '<img src="'+data+'" height="42" width="70" class="img_datatable">';
                        },
                        "targets": 0
                    },
                    { "orderable": false, searchable: false, "targets": 0 },
                    {
                        "render": function (data, type, row) {
                            return '<div class="myEllipsisMd" data-toggle="tooltip" title="'+data+'">'+data+'</div>';
                        },
                        "targets": [2, 3]
                    },
                    {
                        "render": function (data, type, row) {
                            return '<div class="myEllipsisLg" data-toggle="tooltip" title="'+data+'">'+data+'</div>';
                        },
                        "targets": [4]
                    },
                    {
                        "render": function (data, type, row) {
                            return '<div class="myEllipsisXlg" data-toggle="tooltip" title="'+data+'">'+data+'</div>';
                        },
                        "targets": [5]
                    }
                ],
                "buttons": [
                    {
                        text: 'Scrap',
                        action: function (e, dt, node, config) {
                            scrap();
                            feeds_table.ajax.reload();
                        }
                    },
                    {
                        text: 'Refrescar',
                        action: function (e, dt, node, config) {
                            feeds_table.ajax.reload();
                        }
                    },
                    {
                        text: 'Eliminar',
                        attr: {
                            id: 'btn_destroy_modal_feed',
                            class: 'btn_showed_if_feed_selected'
                        },
                        action: function (e, dt, node, config) {
                            $('#destroy_feed_modal').modal('show');
                            id = feeds_table.rows({selected:true}).ids().toArray();
                            getFeedById(id);
                        }
                    },
                    {
                        text: 'Ver',
                        attr: {
                            id: 'btn_show_modal_feed',
                            class: 'btn_showed_if_feed_selected'
                        },
                        action: function (e, dt, node, config) {
                            $('#show_feed_modal').modal('show');
                            id = feeds_table.rows({selected:true}).ids().toArray();
                            getFeedById(id);
                        }
                    },
                    {
                        text: 'Nuevo',
                        attr: { id: 'btn_new_feed' },
                        action: function (e, dt, node, config) {
                            feeds_table.rows().deselect();
                            $('#show_feed_modal').modal('show');
                        }
                    },
                    /*{
                        extend: 'excel', text: 'Exportar', exportOptions: {modifier: {search: 'applied'}}
                    }*/
                ]
            });

            // Apply the search
            feeds_table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

            $("#app").on('click','#feeds_table tr', function() {
                if ($(this).hasClass('selected')) {
                    var id = feeds_table.rows( this  ).ids()[0];
                    console.log( "selection" );
                    $('.btn_showed_if_feed_selected').show();
                } else {
                    var id = null;
                    console.log( "no select" );
                    $('.btn_showed_if_feed_selected').hide();
                }
                console.log(id);
            } );

        })(jQuery);
    </script>
@endsection
