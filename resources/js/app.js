/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueResource from 'vue-resource';
Vue.use(VueResource);

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import welcome from './vue/components/welcome.vue'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: {
        welcome
    }
});


/* Set the defaults for DataTables initialisation */
$.extend( true, $.fn.dataTable.defaults, {
    "language": {
        select: {
            rows: {
                _: "%d filas seleccionadas",
                0: "Click en una fila para seleccionarla",
                1: "1 fila seleccionada"
            }
        },
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    select: true,
    "order": [[ 0, 'asc' ]],
    "processing": true,
    "serverSide": true, // comment in order to allow export of all rows (vissible and hidden)
    //"deferLoading": 0, // This will delay the loading of data until a filter, sorting action or draw/reload Ajax happens programmatically
    "dom":
        "<'d-flex justify-content-around justify-content-md-between flex-wrap'"
        +"<'justify-content-start'f>"
        +"<'justify-content-end justify-content-md-start'B>"
        +"<'justify-content-start'l>"
        +">"
        +"<'row'<'col-sm-12'tr>>"
        +"<'d-flex justify-content-around justify-content-md-between flex-wrap'"
        +"<'justify-content-end'i>"
        +"<'justify-content-end'p>"
        +">",
    "renderer": 'bootstrap'
} );

/*document.oncontextmenu = function() {return false;};*/
