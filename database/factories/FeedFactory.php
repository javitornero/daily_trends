<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Feed;
use Faker\Generator as Faker;

$factory->define(Feed::class, function (Faker $faker) {
    return [
        'publication_id' => $faker->ean13,
        'published_date' => $faker->dateTime,
        'image' => $faker->url,
        'publisher' => $faker->randomElement(['El País', 'El Mundo']),
        'source' => substr($faker->firstName . ' ' . $faker->lastName, 0, 191),
        'title' => $faker->realText(26, 2),
        'subtitle' => $faker->realText(50, 2),
        'body' => $faker->realText(100, 2)
    ];
});
