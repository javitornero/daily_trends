<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Services\ScrapService;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class FeedController extends Controller
{
    /**
     * Process datatables ajax request.
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function datatable()
    {
        return Datatables::of(DB::table('feeds'))->toJson();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('feeds');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id' => 'string|size:36',
            /*'image' => 'file|image|dimensions:min_width=100,min_height=200,max_width=1000,max_height=500,ratio=3/2',*/
            'image' => 'file|image',
            'img_src' => 'string',
            'publisher' => 'required|string|max:191',
            'source' => 'required|string|max:191',
            'published_date' => 'required|date',
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'body' => 'required|string'
        ]);

        $feed = Feed::find($request->id);
        if (!$feed){
            $feed = Feed::create(['publication_id' => time().'_'.rand(0,1000)]);
        }

        if ($request->image) {
            // Delete old image file
            $old_path = explode("/", $feed->image);
            $old_path = array_reverse($old_path);
            $old_file_name = $old_path[0];
            Storage::delete('public/images/'.$old_file_name);

            // Create new image file in storage/app/public/images
            $extension = $request->image->getClientOriginalExtension();
            $file_name = $feed->id . '_' . time() . '.' . $extension;
            $path = $request->file('image')->storeAs(
                'public/images', $file_name
            );
        }
        $feed_from_request = $request->all();
        if (isset($file_name)) {
            $path = 'storage/images/' . $file_name;
            $feed_from_request['image'] = $path;
        } else if ($request->img_src == 'images/default_feed.jpg') {
            $feed_from_request['image'] = 'images/default_feed.jpg';
        } else{
            unset($feed_from_request['image']);
        }
        unset($feed_from_request['img_src']);

        try{
            $feed = Feed::updateOrCreate(['id' => $feed->id], $feed_from_request);

            return response()->json([
                'feed' => $feed,
                'success' => 'Stored Feed'
            ]);
        } catch(QueryException $e){
            Log::error($e->getMessage());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     */
    public function show(Request $request)
    {
        $feed = Feed::find($request->id);

        return response()->json(['feed' => $feed]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     */
    public function destroy(Request $request)
    {
        $feed = Feed::find($request->id);
        $feed->delete();

        return response()->json(['success' => 'Destroyed feed']);
    }

    /**
     * @return JsonResponse
     */
    public function getFeedsOfToday(){
        $scrap_service = new ScrapService();
        $scrap_service->scrap();
        $today = Carbon::today()->startOfDay()->format('Y-m-d H:m:s');
        $feeds_of_today = Feed::where('published_date', '>=', $today)->orderBy('published_date', 'desc')->get();

        return response()->json($feeds_of_today);
    }

    public function scrap(){
        $scrap_service = new ScrapService();

        return $scrap_service->scrap();
    }
}
