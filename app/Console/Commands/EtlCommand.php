<?php

namespace App\Console\Commands;

use App\Services\ScrapService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class EtlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'etl:feeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = microtime(true);
        $scrap_service = new ScrapService();
        $scrap_service->scrap();
        $time = round(microtime(true) - $time, 1);
        $this->info('Scrap done in ' . $time . 's');
        Log::info('***** etl:news '. $time . 's');
    }
}
