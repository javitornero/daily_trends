<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    use \App\Libs\Uuids;

    protected $guarded = ['id'];
}
