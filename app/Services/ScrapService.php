<?php

namespace App\Services;

use App\Feed;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ScrapService
{
    private $urls = [
        'El País' => 'https://elpais.com',
        'El Mundo' => 'https://www.elmundo.es'
    ];

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function scrap(){
        try {
            $feeds = [];
            foreach ($this->urls as $publisher => $url) {
                $feeds_by_publisher = $this->getFeedsByPublisher($publisher, $url);
                foreach ($feeds_by_publisher as $feed) {
                    $feeds[] = $feed;
                }
            }

            $this->storeNewFeeds($feeds);

            return response()->json([
                'success' => 'web scrap done'
            ]);

        }catch(\Exception $e){
            dump($e->getMessage());
            Log::error('etl:news - ' .$e->getMessage());
            die;
        }
    }

    /**
     * @param $publisher
     * @param $welcome_url
     * @return array
     */
    private function getFeedsByPublisher($publisher, $welcome_url){
        $xpath_str_path = null;
        switch ($publisher) {
            case 'El País':
                $xpath_str_path = '//h2[contains(@class,"articulo-titulo")]/a';
                break;
            case 'El Mundo':
                $xpath_str_path = '//header[contains(@class,"ue-c-cover-content__headline-group")]/a[contains(@class,"ue-c-cover-content__link")]';
                break;
        }
        $welcome_xpath = $this->getXPath($welcome_url);
        $article_urls = $this->getArticleUrls($welcome_xpath, $xpath_str_path);
        $feeds = [];
        foreach ($article_urls as $article_url) {
            $article_xpath = $this->getXPath($article_url);
            $feeds[] = $this->getFeed($article_xpath, $publisher, $article_url);
        }

        return $feeds;
    }

    /**
     * @param $url
     * @return \DOMXpath
     */
    private function getXPath($url){
        libxml_use_internal_errors(true);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $dom = new \DOMDocument();
        $response = curl_exec($ch);
        $dom->loadHTML($response);
        $dom->preserveWhiteSpace = false;
        $xpath = new \DOMXpath($dom);
        libxml_clear_errors();

        return $xpath;
    }

    /**
     * @param \DOMXpath $xpath
     * @param $xpath_str_path
     * @return array
     */
    private function getArticleUrls(\DOMXpath $xpath, $xpath_str_path){
        $nodes = $xpath->query($xpath_str_path);
        $urls = [];
        for ($i=0; $i <= 4; $i++){
            $urls[] = $nodes->item($i)->getAttribute('href');
        }

        return $urls;
    }

    /**
     * @param \DOMXpath $xpath
     * @param $publisher
     * @param $article_url
     * @return array
     */
    private function getFeed(\DOMXpath $xpath, $publisher, $article_url){
        $publication_id = $this->getPublicationIdFromUrl($article_url);
        switch ($publisher) {
            case 'El País':
                $feed = $this->getElPaisFeed($xpath, $publication_id);
                break;
            case 'El Mundo':
                $feed = $this->getElMundoFeed($xpath, $publication_id);
                break;
        }
        $feed['publisher'] = $publisher;

        return $feed;
    }

    /**
     * @param $article_url
     * @return mixed
     */
    private function getPublicationIdFromUrl($article_url){
        $url_elements = explode('/', $article_url);
        $url_elements = end($url_elements);
        $url_elements = explode('.', $url_elements);
        $publication_id = $url_elements[0];

        return $publication_id;
    }

    /**
     * @param \DOMXpath $xpath
     * @param $publication_id
     * @return array
     */
    private function getElPaisFeed(\DOMXpath $xpath, $publication_id){
        $feed['publication_id'] = $publication_id;
        $nodes = $xpath->query('//time[contains(@class,"articulo-actualizado")]');
        if ($nodes->count() > 0) {
            $feed['published_date'] = $nodes->item(0)->getAttribute('datetime');
            $feed['published_date'] = Carbon::create($feed['published_date'])->format('Y-m-d H-i:s');
        } else{
            $feed['published_date'] = null;
        }

        $nodes = $xpath->query('//div/figure/img');
        if ($nodes->count() > 0){
            $feed['image'] = 'https:'.$nodes->item(0)->getAttribute('data-src');
        } else {
            $nodes = $xpath->query('//figure/meta[1]/@content');
            $feed['image'] = ($nodes->count() > 0) ? $nodes->item(0)->nodeValue : null;
        }

        $nodes = $xpath->query('//span[contains(@class,"autor-nombre")]/a');
        $feed['source'] = ($nodes->count() > 0) ? ucwords(strtolower($nodes->item(0)->textContent)) : null;
        $nodes = $xpath->query('//h1[contains(@class,"articulo-titulo")]');
        $feed['title'] = ($nodes->count() > 0) ? $nodes->item(0)->textContent : null;
        $nodes = $xpath->query('//h2[contains(@class,"articulo-subtitulo")]');
        if ($nodes->count() > 0) {
            $feed['subtitle'] = $nodes->item(0)->textContent;
        } else {
            $nodes = $xpath->query('//div[contains(@id,"articulo-introduccion")]/p');
            $feed['subtitle'] = ($nodes->count() > 0) ? $nodes->item(0)->textContent : null;
        }
        $nodes = $xpath->query('//div[contains(@class,"articulo-cuerpo")]/p');
        $feed['body'] = '';
        foreach ($nodes as $node) {
            $feed['body'] .=  ' '. $node->textContent;
        }

        return $feed;
    }

    /**
     * @param \DOMXpath $xpath
     * @param $publication_id
     * @return array
     */
    private function getElMundoFeed(\DOMXpath $xpath, $publication_id){
        $feed['publication_id'] = $publication_id;
        $nodes = $xpath->query('//div[contains(@class,"ue-c-article__publishdate")]/time');
        if ($nodes->count() > 0){
            $feed['published_date'] = $nodes->item(0)->getAttribute('datetime');
            $feed['published_date'] = Carbon::create($feed['published_date'])->format('Y-m-d H-i:s');
        }
        $nodes = $xpath->query('//img[contains(@class,"ue-c-article__media--image")]');
        if ($nodes->count() > 0){
            $feed['image'] = $nodes->item(0)->getAttribute('src');
        } else if ($nodes->count() == 0){
            $nodes = $xpath->query('//figure[contains(@class,"ue-c-article__media ue-c-article__media--video ue-l-article--edge-left-n-right-until-tablet ue-c-article--media-icon-m-from-phablet")]/meta[3]');
            if ($nodes->count() > 0) {
                $feed['image'] = $nodes->item(0)->attributes->item(1)->childNodes->item(0)->nodeValue;
            }
        }
        $nodes = $xpath->query('//div[contains(@class,"ue-c-article__byline-name")]');
        $feed['source'] = ($nodes->count() > 0) ? ucwords(strtolower($nodes->item(0)->textContent)) : null;
        $nodes = $xpath->query('//h1[contains(@class,"ue-c-article__headline")]');
        $feed['title'] = ($nodes->count() > 0) ? $nodes->item(0)->textContent : null;
        $nodes = $xpath->query('//p[contains(@class,"ue-c-article__standfirst")]');
        $feed['subtitle'] = ($nodes->count() > 0) ? $nodes->item(0)->textContent : null;
        $nodes = $xpath->query('//div[contains(@class,"ue-l-article__body ue-c-article__body")]/p');

        $feed['body'] = '';
        foreach ($nodes as $node) {
            $feed['body'] .= ' ' . $node->textContent;
        }

        return $feed;
    }

    private function storeNewFeeds($feeds){
        $db_feeds = DB::table('feeds')->orderBy('published_date', 'desc')->get()->toArray();
        foreach ($feeds as $feed) {
            $exists = false;
            foreach ($db_feeds as $db_feed) {
                if ($db_feed->publication_id === $feed['publication_id']){
                    $exists = true;
                    break;
                }
            }
            if (!$exists){
                Feed::create($feed);
            }
        }
    }
}
